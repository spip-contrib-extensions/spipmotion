<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/spipmotion.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'spipmotion_description' => 'Faire un site à la Youtube ou Dailymotion ...',
	'spipmotion_slogan' => 'Encodage de documents multimedia'
);
